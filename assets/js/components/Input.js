/*
 * The MIT License
 *
 * Copyright 2021 Serhiy Yakymenko <serh.yakymenko@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import React, {Component} from 'react';
import axios from 'axios';

class Input extends Component {
    constructor(props) {
        super(props);
        this.state = {
          min: '',
          max: '',
          count: '',
          error: '',
          loading: false};
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
      switch (event.target.name) {
        case 'min':
          this.setState({min: event.target.value});
          break;
        case 'max':
          this.setState({max: event.target.value});
          break;
        case 'count':
          this.setState({count: event.target.value});
          break;
      }
    }

    handleSubmit(event) {
      event.preventDefault();
      this.props.changeUid('');
      this.postInput();
    }

    componentDidMount() {
//        this.postInput();
    }

    postInput() {
      this.setState({loading: true});
      axios.post(`http://amm.junioruman.com.ua/api/count`,{
        min: this.state.min,
        max: this.state.max,
        count: this.state.count
      }).then(res => {
        this.setState({
          error: res.data.error,
          loading: false});
        console.log(res.data.uid);
        this.props.changeUid(res.data.uid);
      })
    }

    render() {
        const loading = this.state.loading;
        return(
            <div>
                <section className="row-section">
                    <div className="container">
                        <div className="row">
                            <h2 className="text-center"><span>Calculations</span> Created  <i
                                className="fa fa-edit"></i> by Serhiy Yakymenko</h2>
                        </div>
                        {loading ? (
                            <div className={'row text-center'}>
                                <span className="fa fa-spin fa-spinner fa-4x"></span>
                            </div>
                        ) : (
                            <div className={'row text-center'}>
                                <form onSubmit={this.handleSubmit}>
                                    <div className="col-xs-12 row-block">
                                        <ul>
                                            <li>
                                                <div className="media">
                                                    <div className="media-body">
                                                        <label>
                                                          <h4>Minimum Value</h4>
                                                          <input autoFocus type="number" value={this.state.min} onChange={this.handleChange} name="min" />
                                                        </label>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="col-xs-12 row-block">
                                        <ul>
                                            <li>
                                                <div className="media">
                                                    <div className="media-body">
                                                        <label>
                                                          <h4>Maximum Value</h4>
                                                          <input type="number" value={this.state.max} onChange={this.handleChange} name="max" />
                                                        </label>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="col-xs-12 row-block">
                                        <ul>
                                            <li>
                                                <div className="media">
                                                    <div className="media-body">
                                                        <label>
                                                          <h4>Numbers Count</h4>
                                                          <input type="number" value={this.state.count} onChange={this.handleChange} name="count" />
                                                        </label>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="col-xs-12 row-block">
                                        <ul>
                                            <li>
                                                <div className="media">
                                                    <div className="media-body">
                                                        <label>
                                                          <h4>Start Processing</h4>
                                                          <input className="btn-default" type="submit" value="Process" />
                                                        </label>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>

                                </form>
                            </div>
                        )}
                    </div>
                </section>
            </div>
        )
    }
}
export default Input;
