/*
 * The MIT License
 *
 * Copyright 2021 Serhiy Yakymenko <serh.yakymenko@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import React, {Component} from 'react';
import {Route, Switch, Redirect, Link, withRouter} from 'react-router-dom';
import Input from './Input';
import Calculations from './Calculations';

class Home extends Component {
    constructor() {
        super();
        this.state = {
          status: '',
          uid: ''
        };
        this.changeUid = uid => {
          this.setState({
            uid: uid
          });
        }
        this.changeStatus = status => {
          this.setState({
            status: status
          });
        }

    }
    render() {
        return (
           <div>
               <nav className="navbar navbar-dark bg-dark justify-content-center">
               <div className="navbar justify-content-center" ><Link className={"navbar-brand"} to={"/"}> Symfony React Project </Link></div>
               </nav>

               <Input changeUid={this.changeUid} />
               <Calculations uid={this.state.uid} />
           </div>
        )
    }
}

export default Home;
