/*
 * The MIT License
 *
 * Copyright 2021 Serhiy Yakymenko <serh.yakymenko@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import React, {Component} from 'react';
import axios from 'axios';


class Calculations extends Component {
    constructor(props) {
        super(props);
        this.state = {
//          uid: '',
          status: '',
          avg: '',
          stdev: '',
          moda: '',
          med: '',
          loading: false
        }
    }

    componentDidMount() {
      this.interval = setInterval(() => this.needToGo(), 1000);
    }

    componentWillUnmount() {
      clearInterval(this.interval);
    }

    needToGo() {
      if (this.props.uid=='') {
        this.setState({status: '',
              avg: '',
              stdev: '',
              moda: '',
              med: ''});
      }
      if (this.props.uid!='' && this.state.status != 'stddevdone') {
//        alert('GO3');
        this.getStatus();
      }
      if (this.state.status == 'stddevdone') {
        clearInterval(this.interval);
      }
    }

    getStatus() {
        this.setState({loading: true});
        axios.post(`http://amm.junioruman.com.ua/api/status`, {
            uid: this.props.uid
        }).then(res => {
            const status = res.data.status;
            const avg = res.data.avg;
            const stdev = res.data.stdev;
            const moda = res.data.moda;
            const med = res.data.med;
//          console.log(res.data);
            this.setState({status: status,
              avg: avg,
              stdev: stdev,
              moda: moda,
              med: med,
              loading: false })
        })
    }

    render() {
        const loading = this.state.loading;
        const status = this.state.status;
        const avg = this.state.avg;
        const stdev = this.state.stdev;
        return (
            <div>
                <section className="row-section">
                    <div className="container">

                        {loading ? (
                            <div className={'row text-center'}>
                                <span className="fa fa-spin fa-spinner fa-4x"></span>
                            </div>

                        ) : (
                            <div className={'row text-center'}>

                                    <div className="col-xs-12 row-block">
                                        <ul>
                                            <li>
                                                <div className="media">
                                                    <div className="media-body">
                                                        <h4>Status</h4>
                                                        <p>{status}</p>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="col-xs-12 row-block">
                                        <ul>
                                            <li>
                                                <div className="media">
                                                    <div className="media-body">
                                                        <h4>Average</h4>
                                                        <p>{avg}</p>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="col-xs-12 row-block">
                                        <ul>
                                            <li>
                                                <div className="media">
                                                    <div className="media-body">
                                                        <h4>Standart Deviation</h4>
                                                        <p>{stdev}</p>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>

                            </div>
                        )}
                    </div>
                </section>
            </div>
        )
    }
}

export default Calculations;
