<?php

namespace App\Repository;

use App\Entity\Digits;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Digits|null find($id, $lockMode = null, $lockVersion = null)
 * @method Digits|null findOneBy(array $criteria, array $orderBy = null)
 * @method Digits[]    findAll()
 * @method Digits[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DigitsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Digits::class);
    }

    public function flushAll(): void
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'DELETE FROM `digits`';
        $stmt = $conn->prepare($sql);
        $stmt->execute();

    }

    public function resetCounter(): void
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'ALTER TABLE `digits` AUTO_INCREMENT = 1';
        $stmt = $conn->prepare($sql);
        $stmt->execute();
    }

    public function getAverage()
    {
        $qb = $this->createQueryBuilder('d');
        return $qb->select("avg(d.v)")
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getStDeviation()
    {
        $qb = $this->createQueryBuilder('d');
        return $qb->select("STDDEV(d.v)")
            ->getQuery()
            ->getSingleScalarResult();
    }

}
