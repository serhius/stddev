<?php

namespace App\Service;

/*
 * The MIT License
 *
 * Copyright 2021 Serhiy Yakymenko <serh.yakymenko@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

use Symfony\Component\HttpClient\HttpClient;

/**
 * Making requests for processing
 *
 * @author Serhiy Yakymenko <serh.yakymenko@gmail.com>
 */
class GenerateDigits
{
    private $client;

    public function __construct()
    {
        $this->client = HttpClient::create();
    }

    public function generate(int $min, int $max, int $count, int $continueFrom = 0): void
    {
        $response = $this->client->request(
            'POST',
            'http://'.$_ENV['MAIN_URL'].$_ENV['GEN_URI'],
            [
                'body'=>[
                    'min'=>$min,
                    'max'=>$max,
                    'count'=>$count,
                    'continueFrom'=>$continueFrom
                ]
            ]);
    }

    public function avg(): void
    {
        $response = $this->client->request(
            'GET',
            'http://'.$_ENV['MAIN_URL'].$_ENV['AVG_URI']
        );
    }

    public function stDev(): void
    {
        $response = $this->client->request(
            'GET',
            'http://'.$_ENV['MAIN_URL'].$_ENV['STDDEV_URI']
        );
    }

    public function moda(): void
    {
        $response = $this->client->request(
            'GET',
            'http://'.$_ENV['MAIN_URL'].$_ENV['MODA_URI']
        );
    }

    public function median(): void
    {
        $response = $this->client->request(
            'GET',
            'http://'.$_ENV['MAIN_URL'].$_ENV['MEDIAN_URI']
        );
    }

}
