<?php

namespace App\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\GenerateDigits;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Digits;
use App\Entity\Calculation;
use App\Repository\DigitsRepository;
use App\Repository\CalculationRepository;

/**
 * @Route("/", name="root")
 */
class DefaultController extends AbstractController
{
    /**
     * @Route("/api/count", name="count", methods="POST")
     */
    public function count(Request $request, CalculationRepository $calculations, DigitsRepository $digits, LoggerInterface $logger): JsonResponse
    {
        $response = new JsonResponse();
        $responseData = Array('error'=>'','uid'=>'');
        $em = $this->getDoctrine()->getManager();

        if ($request->request->has('min')) {
            $min = $request->request->get('min');
        } elseif (property_exists(json_decode($request->getContent()), 'min')) {
            $min = json_decode($request->getContent())->min;
        }
        if ($request->request->has('max')) {
            $max = $request->request->get('max');
        } elseif (property_exists(json_decode($request->getContent()), 'max')) {
            $max = json_decode($request->getContent())->max;
        }
        if ($request->request->has('count')) {
            $count = $request->request->get('count');
        } elseif (property_exists(json_decode($request->getContent()), 'count')) {
            $count = json_decode($request->getContent())->count;
        }
        if (!(isset($min) && isset($max) && isset($count))) {
            $responseData['error'] = 'empty input';
            $response->setData($responseData);
            return $response;
        }
        if (true && $_ENV['APP_ENV'] == 'dev' ) {
            $allCalcs = $calculations->findAll();
            foreach ($allCalcs as $calc) {
                $em->remove($calc);
            }
            $em->flush();
            $digits->flushAll();
            $digits->resetCounter();
        } else {
            $calcsInProgress = $calculations->findNotReady('stddevdone');
            if ($calcsInProgress && count($calcsInProgress)) {
                $responseData['error'] = 'calculation in progress';
                $response->setData($responseData);
                return $response;
            } else {
                $digits->flushAll();
                $digits->resetCounter();
            }
        }
        $calculation = new Calculation();
        $calculation->setMin(intval($min));
        $calculation->setMax(intval($max));
        $calculation->setCount(intval($count));
        $uid = uniqid($count.$max.$min);
        $calculation->setUid($uid);
        $calculation->setStatus('new');
        $em->persist($calculation);
        $em->flush($calculation);

        (new GenerateDigits())->generate(intval($min), intval($max), intval($count));

        $responseData['uid'] = $uid;
        $response->setData($responseData);
        return $response;
    }

    /**
     * @Route("/api/generate", name="generate", methods="POST")
     */
    public function generate(Request $request, CalculationRepository $calculations, DigitsRepository $digits, LoggerInterface $logger): JsonResponse
    {
/*
//        @TODO enable time spliting
//        $timeLimit = 30;
//        set_time_limit($timeLimit);
//        ini_set('max_execution_time', $timeLimit);
 */
        $start = microtime(true);
        $response =  new JsonResponse();

        if ($request->request->has('min')) {
            $min = $request->request->get('min');
        }
        if ($request->request->has('max')) {
            $max = $request->request->get('max');
        }
        if ($request->request->has('count')) {
            $count = $request->request->get('count');
        }
        if ($request->request->has('continueFrom')) {
            $continueFrom = $request->request->get('continueFrom');
        } else {
            $continueFrom = 0;
        }
        if (!(isset($min) && isset($max) && isset($count))) {
            $responseData['error'] = 'empty input';
            $response->setData($responseData);
            return $response;
        }
        $em = $this->getDoctrine()->getManager();
//  @TODO make while      for ($c=intval($continueFrom);($c<intval($count))&&(($timeLimit-3)>(microtime(true)-$start));$c++ ) {
        for ($c=intval($continueFrom);$c<intval($count);$c++ ) {
            $digit = new Digits();
            $digit->setV(rand(intval($min), intval($max)));
            $em->persist($digit);
        }
        $em->flush();
/*
//        @TODO enable time spliting
//        if ($c<intval($count)) {
//            $genDigits = new GenerateDigits();
//            $genDigits->generate(intval($min), intval($max), intval($count), $c);
//        } else {
*/
            $currentCalc = $calculations->findOneBy(['status'=>'new']);
            if (null !== $currentCalc) {
                $currentCalc->setStatus('generated');
                $em->persist($currentCalc);
                $em->flush($currentCalc);
            }
            $end = microtime(true);
            (new GenerateDigits())->avg();

/*
//        }
*/
        $responseData['status'] = 'Generating Done';
        $responseData['start'] = $start;
        $responseData['end'] = $end;
        $logger->info('/generate', $responseData);
        $response->setData($responseData);
        return $response;
    }


    /**
     * @Route("/api/status", name="status", methods="GET|POST")
     */
    public function getStatus(Request $request, CalculationRepository $calculations, LoggerInterface $logger): JsonResponse
    {
        $response =  new JsonResponse();

        if ($request->request->has('uid')) {
            $uid = $request->request->get('uid');
        } elseif ($request->query->has('uid')) {
            $uid = $request->query->get('uid');
        } elseif (property_exists(json_decode($request->getContent()), 'uid')) {
            $uid = json_decode($request->getContent())->uid;
        } else {
            $responseData['status'] = 'bad';
            $response->setData($responseData);
            return $response;
        }
        $calculation = $calculations->findByUid($uid);
        $response->setData([
            'status' => $calculation->getStatus(),
            'avg' => $calculation->getAvg(),
            'stdev' => $calculation->getStdev(),
            'moda' => $calculation->getModa(),
            'med' => $calculation->getMed()
        ]);
        return $response;
    }

    /**
     * @Route("/api/avg", name="avg", methods="GET|POST")
     */
    public function avg(DigitsRepository $digits, CalculationRepository $calculations, LoggerInterface $logger): JsonResponse
    {
        $response =  new JsonResponse();
        $start = microtime(true);
        $em = $this->getDoctrine()->getManager();
        $average = $digits->getAverage();
        $currentCalc = $calculations->findOneBy(['status'=>'generated']);
        $currentCalc->setAvg(floatval($average));
        $currentCalc->setStatus('avgdone');
        $em->persist($currentCalc);
        $em->flush($currentCalc);
        $end = microtime(true);
        (new GenerateDigits())->stDev();
        $responseData['status'] = 'Find average';
        $responseData['avg'] = $average;
        $responseData['start'] = $start;
        $responseData['end'] = $end;
        $logger->info('AVG', $responseData);
        $response->setData($responseData);
        return $response;
    }

    /**
     * @Route("/api/stdev", name="stdev", methods="GET|POST")
     */
    public function stDev(DigitsRepository $digits, CalculationRepository $calculations, LoggerInterface $logger): JsonResponse
    {
        $response =  new JsonResponse();
        $start = microtime(true);
        $em = $this->getDoctrine()->getManager();
        $stdDev = $digits->getStDeviation();
        $currentCalc = $calculations->findOneBy(['status'=>'avgdone']);
        $currentCalc->setStdev(floatval($stdDev));
        $currentCalc->setStatus('stddevdone');
        $em->persist($currentCalc);
        $em->flush($currentCalc);
        $end = microtime(true);
//        (new GenerateDigits())->moda();
        $responseData['status'] = 'Find standart deviation';
        $responseData['stdev'] = $stdDev;
        $responseData['start'] = $start;
        $responseData['end'] = $end;
        $logger->info('STDDEV', $responseData);
        $response->setData($responseData);
        return $response;
    }

    /**
     * @Route("/api/moda", name="moda", methods="GET|POST")
     */
    public function moda(DigitsRepository $digits, CalculationRepository $calculations, LoggerInterface $logger): JsonResponse
    {
        $response =  new JsonResponse();
        $start = microtime(true);
        $em = $this->getDoctrine()->getManager();
        $moda = $digits->getStDeviation();
        $currentCalc = $calculations->findOneBy(['status'=>'stddevdone']);
        $currentCalc->setModa(floatval($moda));
        $currentCalc->setStatus('modadone');
        $em->persist($currentCalc);
        $em->flush($currentCalc);
        $end = microtime(true);
//        (new GenerateDigits())->median();
        $responseData['status'] = 'Find moda';
        $responseData['moda'] = $moda;
        $responseData['start'] = $start;
        $responseData['end'] = $end;
        $logger->info('MODA', $responseData);
        $response->setData($responseData);
        return $response;

    }

    /**
     * @Route("/api/median", name="median", methods="GET|POST")
     */
    public function median(DigitsRepository $digits, CalculationRepository $calculations, LoggerInterface $logger): JsonResponse
    {
        $response =  new JsonResponse();
        $start = microtime(true);
        $em = $this->getDoctrine()->getManager();
        $median = $digits->getStDeviation();
        $currentCalc = $calculations->findOneBy(['status'=>'modadone']);
        $currentCalc->setMed(floatval($median));
        $currentCalc->setStatus('meddone');
        $em->persist($currentCalc);
        $em->flush($currentCalc);
        $end = microtime(true);
        $responseData['status'] = 'Find median';
        $responseData['median'] = $median;
        $responseData['start'] = $start;
        $responseData['end'] = $end;
        $logger->info('MEDIAN', $responseData);
        $response->setData($responseData);
        return $response;
    }

    /**
     * @Route("/{reactRouting}", name="home", defaults={"reactRouting": null})
     */
    public function index(): Response
    {
        return $this->render('default/index.html.twig', [
//            'controller_name' => 'DefaultController',
        ]);
    }

}
