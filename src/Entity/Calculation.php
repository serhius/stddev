<?php

namespace App\Entity;

use App\Repository\CalculationRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Entity(repositoryClass=CalculationRepository::class)
 * @ORM\Table(indexes={@ORM\Index(name="uid_idx", columns={"uid"})})
 */
class Calculation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $min;

    /**
     * @ORM\Column(type="integer")
     */
    private $max;

    /**
     * @ORM\Column(type="integer")
     */
    private $count;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $uid;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $avg;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $stdev;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $moda;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $med;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $status;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMin(): ?int
    {
        return $this->min;
    }

    public function setMin(int $min): self
    {
        $this->min = $min;

        return $this;
    }

    public function getMax(): ?int
    {
        return $this->max;
    }

    public function setMax(int $max): self
    {
        $this->max = $max;

        return $this;
    }

    public function getCount(): ?int
    {
        return $this->count;
    }

    public function setCount(int $count): self
    {
        $this->count = $count;

        return $this;
    }

    public function getUid(): ?string
    {
        return $this->uid;
    }

    public function setUid(?string $uid): self
    {
        $this->uid = $uid;

        return $this;
    }

    public function getAvg(): ?float
    {
        return $this->avg;
    }

    public function setAvg(?float $avg): self
    {
        $this->avg = $avg;

        return $this;
    }

    public function getStdev(): ?float
    {
        return $this->stdev;
    }

    public function setStdev(?float $stdev): self
    {
        $this->stdev = $stdev;

        return $this;
    }

    public function getModa(): ?float
    {
        return $this->moda;
    }

    public function setModa(?float $moda): self
    {
        $this->moda = $moda;

        return $this;
    }

    public function getMed(): ?float
    {
        return $this->med;
    }

    public function setMed(?float $med): self
    {
        $this->med = $med;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }
}
