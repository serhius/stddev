<?php

namespace App\Entity;

use App\Repository\DigitsRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Entity(repositoryClass=DigitsRepository::class)
 * @ORM\Table(indexes={@ORM\Index(name="v_idx", columns={"v"})})
 */
class Digits
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $v;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getV(): ?int
    {
        return $this->v;
    }

    public function setV(int $v): self
    {
        $this->v = $v;

        return $this;
    }
}
